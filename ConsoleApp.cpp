#include <iostream>

class Animal
{
public:
    virtual void voice()
    {
        std::cout << "Animal voice\n";
    }
};

class Dog : public Animal
{
public:
    void voice() override
    {
        std::cout << "Woof!\n";
    }
};

class Cat : public Animal
{
public:
    void voice() override
    {
        std::cout << "Meow!\n";
    }
};

class Goose : public Animal
{
public:
    void voice() override
    {
        std::cout << "Honk!\n";
    }
};


int main()
{
    Animal* animals[3];
    animals[0] = new Dog();
    animals[1] = new Cat();
    animals[2] = new Goose();
    
    for (auto& animal : animals)
    {
        animal->voice();
    }

    for (auto animal : animals)
    {
        delete animal;
    }
}

